﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//for debug on android
using UnityEngine.UI;

struct FluidParticle
{
    public Vector2 vPosition;
    public Vector2 vVelocity;
}

struct FluidParticleDensity
{
    public float fDensity;
}

struct FluidParticleForces
{
    public Vector2 vAcceleration;
}

public class Fluid2D : MonoBehaviour {
    // Compute Shader Constants
    // Grid cell key size for sorting, 8-bits for x and y
    const int NUM_GRID_INDICES = 65536;

    // Numthreads size for the simulation
    const int SIMULATION_BLOCK_SIZE = 256;

    // Numthreads size for the sort
    const int BITONIC_BLOCK_SIZE = 512;
    const int TRANSPOSE_BLOCK_SIZE = 16;

    // For this sample, only use power-of-2 numbers >= 8K and <= 64K
    // The algorithm can be extended to support any number of particles
    // But to keep the sample simple, we do not implement boundary conditions to handle it
    const int NUM_PARTICLES_8K = 8 * 1024;
    const int NUM_PARTICLES_16K = 16 * 1024;
    const int NUM_PARTICLES_32K = 32 * 1024;
    const int NUM_PARTICLES_64K = 64 * 1024;
    int g_iNumParticles = NUM_PARTICLES_16K;

    // Particle Properties
    // These will control how the fluid behaves
    float g_fInitialParticleSpacing = 0.0045f;
    float g_fSmoothlen = 0.012f;
    float g_fPressureStiffness = 200.0f;
    float g_fRestDensity = 1000.0f;
    float g_fParticleMass = 0.0002f;
    float g_fViscosity = 0.1f;
    float g_fMaxAllowableTimeStep = 0.005f;
    float g_fParticleRenderSize = 0.003f;

    float g_fGravityMagnitude=0.98f;
    Vector4 g_vGravity=new Vector4(0,-0.98f,0,0);

    // Map Size
    // These values should not be larger than 256 * fSmoothlen
    // Since the map must be divided up into fSmoothlen sized grid cells
    // And the grid cell is used as a 16-bit sort key, 8-bits for x and y
    float g_fMapHeight = 1.2f;
    //float g_fMapWidth = (4.0f / 3.0f) * g_fMapHeight;
    float g_fMapWidth;

    // Map Wall Collision Planes
    float g_fWallStiffness = 3000.0f;
    Vector4[] g_vPlanes=new Vector4[4];

    public ComputeShader fluidCS;
    public ComputeShader sortCS;
    public Material fluidMtrl;
    public Camera orthoCamera;
    //for debug on android
    public Text debugText;
    private FluidParticle[] debugData = new FluidParticle[5];

    private BitonicSorter sorter;

    //structured buffers
    ComputeBuffer cbParticles;
    ComputeBuffer cbSortedParticles;
    ComputeBuffer cbParticleDensity;
    ComputeBuffer cbParticleForces;
    ComputeBuffer cbGrid;
    ComputeBuffer cbGridIndices;

    int BuildGridKernelHandle;
    int ClearGridIndicesKernelHandle;
    int BuildGridIndicesKernelHandle;
    int RearrangeParticlesKernelHandle;
    int DensityKernelHandle;
    int ForceKernelHandle;
    int IntegrateKernelHandle;

    void InitFluidCS()
    {
        BuildGridKernelHandle = fluidCS.FindKernel("BuildGridCS");
        ClearGridIndicesKernelHandle = fluidCS.FindKernel("ClearGridIndicesCS");
        BuildGridIndicesKernelHandle = fluidCS.FindKernel("BuildGridIndicesCS");
        RearrangeParticlesKernelHandle = fluidCS.FindKernel("RearrangeParticlesCS");
        DensityKernelHandle = fluidCS.FindKernel("DensityCS_Grid");
        ForceKernelHandle = fluidCS.FindKernel("ForceCS_Grid");
        IntegrateKernelHandle = fluidCS.FindKernel("IntegrateCS");

        cbParticles=new ComputeBuffer(g_iNumParticles,16);
        cbSortedParticles=new ComputeBuffer(g_iNumParticles,16);
        cbParticleDensity=new ComputeBuffer(g_iNumParticles,4);
        cbParticleForces=new ComputeBuffer(g_iNumParticles,8);
        cbGrid=new ComputeBuffer(g_iNumParticles,4);
        cbGridIndices=new ComputeBuffer(1<<16,8);

        FluidParticle[] temp = new FluidParticle[g_iNumParticles];
        int startingWidth = (int)Mathf.Sqrt((float)g_iNumParticles);
        for (int i = 0; i < g_iNumParticles; i++)
        {
            int x = i % startingWidth;
            int y = i / startingWidth;
            temp[i].vPosition = new Vector2(g_fInitialParticleSpacing * (float)x, g_fInitialParticleSpacing * (float)y);
        }
        cbParticles.SetData(temp);

        fluidCS.SetInt("g_iNumParticles",g_iNumParticles);
        fluidCS.SetFloat("g_fSmoothlen",g_fSmoothlen);
        fluidCS.SetFloat("g_fPressureStiffness",g_fPressureStiffness);
        fluidCS.SetFloat("g_fRestDensity",g_fRestDensity);
        fluidCS.SetFloat("g_fDensityCoef",g_fParticleMass * 315.0f / (64.0f * Mathf.PI * Mathf.Pow(g_fSmoothlen, 9)));
        fluidCS.SetFloat("g_fGradPressureCoef",g_fParticleMass * -45.0f / (Mathf.PI * Mathf.Pow(g_fSmoothlen, 6)));
        fluidCS.SetFloat("g_fLapViscosityCoef",g_fParticleMass * g_fViscosity * 45.0f / (Mathf.PI * Mathf.Pow(g_fSmoothlen, 6)));
        //fluidCS.SetFloat("fWallStiffness",g_fWallStiffness);
        //。。。。。。sao ni ma！
        fluidCS.SetFloat("g_fWallStiffness",g_fWallStiffness);
        //world space to grid space
        fluidCS.SetVector("g_vGridDim",new Vector4(1.0f / g_fSmoothlen,1.0f / g_fSmoothlen,0f,0f));
        fluidCS.SetVector("g_vPlanes0",g_vPlanes[0]);
        fluidCS.SetVector("g_vPlanes1",g_vPlanes[1]);
        fluidCS.SetVector("g_vPlanes2",g_vPlanes[2]);
        fluidCS.SetVector("g_vPlanes3",g_vPlanes[3]);
       
    }

    void Simulation()
    {
        int particlesGroupLayoutX = g_iNumParticles / SIMULATION_BLOCK_SIZE;
        //build grid
        fluidCS.SetBuffer(BuildGridKernelHandle,"GridRW",cbGrid);
        fluidCS.SetBuffer(BuildGridKernelHandle,"ParticlesRO",cbParticles);
        fluidCS.Dispatch(BuildGridKernelHandle,particlesGroupLayoutX, 1, 1);
        //gpu sort
        sorter.GPUSort();
        //clear grid indices
        fluidCS.SetBuffer(ClearGridIndicesKernelHandle,"GridIndicesRW",cbGridIndices);
        fluidCS.Dispatch(ClearGridIndicesKernelHandle, cbGridIndices.count, 1, 1);
        //build grid indices
        fluidCS.SetBuffer(BuildGridIndicesKernelHandle,"GridRO",cbGrid);
        fluidCS.SetBuffer(BuildGridIndicesKernelHandle,"GridIndicesRW",cbGridIndices);
        fluidCS.Dispatch(BuildGridIndicesKernelHandle, particlesGroupLayoutX, 1, 1);
        //rearrange particles
        fluidCS.SetBuffer(RearrangeParticlesKernelHandle,"GridRO",cbGrid);
        fluidCS.SetBuffer(RearrangeParticlesKernelHandle,"ParticlesRO",cbParticles);
        fluidCS.SetBuffer(RearrangeParticlesKernelHandle,"ParticlesRW",cbSortedParticles);
        fluidCS.Dispatch(RearrangeParticlesKernelHandle,particlesGroupLayoutX,1,1);
        //density calculation
        fluidCS.SetBuffer(DensityKernelHandle,"ParticlesRO",cbSortedParticles);
        fluidCS.SetBuffer(DensityKernelHandle,"GridIndicesRO",cbGridIndices);
        fluidCS.SetBuffer(DensityKernelHandle,"ParticlesDensityRW",cbParticleDensity);
        fluidCS.Dispatch(DensityKernelHandle,particlesGroupLayoutX,1,1);
        //force calculation
        fluidCS.SetBuffer(ForceKernelHandle,"ParticlesRO",cbSortedParticles);
        fluidCS.SetBuffer(ForceKernelHandle,"ParticlesDensityRO",cbParticleDensity);
        fluidCS.SetBuffer(ForceKernelHandle,"GridIndicesRO",cbGridIndices);
        fluidCS.SetBuffer(ForceKernelHandle,"ParticlesForcesRW",cbParticleForces);
        fluidCS.Dispatch(ForceKernelHandle,particlesGroupLayoutX,1,1);
        //integration
        fluidCS.SetVector("g_vGravity",g_vGravity);
        fluidCS.SetFloat("g_fTimeStep",Mathf.Min(g_fMaxAllowableTimeStep,Time.deltaTime));
        fluidCS.SetBuffer(IntegrateKernelHandle,"ParticlesRO",cbSortedParticles);
        fluidCS.SetBuffer(IntegrateKernelHandle,"ParticlesForcesRO",cbParticleForces);
        fluidCS.SetBuffer(IntegrateKernelHandle,"ParticlesRW",cbParticles);
        fluidCS.Dispatch(IntegrateKernelHandle, particlesGroupLayoutX, 1, 1);
    }





	void Start () 
    {
        //Debug.Log("support cs:" + (SystemInfo.supportsComputeShaders ? "yes" : "no"));
        InitWallsAndCamera();
        InitFluidCS();
        sorter=new BitonicSorter(cbGrid,sortCS);
	}
	
    void InitWallsAndCamera()
    {
        //g_fMapWidth = 4.0f / 3.0f * g_fMapHeight;
        float aspect=Screen.width/Screen.height;
        //float aspect=4.0f / 3.0f;
        g_fMapWidth = aspect * g_fMapHeight;

        if (!orthoCamera.orthographic)
            orthoCamera.orthographic = true;
        orthoCamera.orthographicSize = g_fMapHeight/2.0f;//参数指的是一半
        orthoCamera.aspect = aspect;
        orthoCamera.transform.position = new Vector3(g_fMapWidth*0.5f,g_fMapWidth*0.5f,-2);

        g_vPlanes[0] = new Vector4(1, 0, 0,0);
        g_vPlanes[1] = new Vector4(0, 1, 0,0);
        g_vPlanes[2] = new Vector4(-1, 0, g_fMapWidth,0);
        g_vPlanes[3] = new Vector4(0, -1, g_fMapHeight,0);
    }

	void Update ()
    {
        UpdateGravity();
        Simulation();
        //DebugOnAndroid();//——》手机上dispatch成功，在AndroidTest项目进几步debug发现，TM单纯是我手机不支持GS的问题。。。
	}

    void UpdateGravity()
    {
        float xAngle=Input.acceleration.x*Mathf.PI/2.0f;//debug发现是linear
        Vector2 direction=new Vector2(Mathf.Sin(xAngle),-Mathf.Cos(xAngle));
        direction = direction.normalized * g_fGravityMagnitude;
        g_vGravity.x = direction.x;
        g_vGravity.y = direction.y;
    }

    void DebugOnAndroid()
    {
        cbParticles.GetData(debugData);
        string headTex="monitor the first 5 particles:\n";
        //一定要除回g_fInitialParticleSpacing，不然太小了都显示为0
        debugText.text = headTex + (debugData[0].vPosition/g_fInitialParticleSpacing).ToString() + "\n"
            + (debugData[1].vPosition/g_fInitialParticleSpacing).ToString() + "\n"
            + (debugData[2].vPosition/g_fInitialParticleSpacing).ToString() + "\n"
            + (debugData[3].vPosition/g_fInitialParticleSpacing).ToString() + "\n"
            + (debugData[4].vPosition/g_fInitialParticleSpacing).ToString();
    }

    void OnRenderObject()
    {
        fluidMtrl.SetPass(0);
        fluidMtrl.SetFloat("g_fParticleSize", g_fParticleRenderSize);
        fluidMtrl.SetBuffer("ParticlesRO",cbParticles);
        fluidMtrl.SetBuffer("ParticleDensityRO",cbParticleDensity);
        Graphics.DrawProcedural(MeshTopology.Points,g_iNumParticles);
    }

    void  OnDestroy()
    {
        //release cbs
        cbGridIndices.Release();
        cbGrid.Release();
        cbSortedParticles.Release();
        cbParticleForces.Release();
        cbParticleDensity.Release();
        cbParticles.Release();
    }

}
