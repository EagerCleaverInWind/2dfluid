﻿Shader "Unlit/FluidRender"
{
	Properties
	{

	}
	SubShader
	{
		
		Pass
		{
			ZTest Always
			ZWrite Off

			CGPROGRAM
			#pragma vertex ParticleVS
			#pragma geometry  ParticleGS
			#pragma fragment ParticlePS
			#pragma target 5.0
			
			#include "UnityCG.cginc"

			struct Particle
			{
    			float2 position;
    			float2 velocity;
			};

			struct ParticleDensity 
			{
   				 float density;
			};

			StructuredBuffer<Particle> ParticlesRO : register( t0 );
			StructuredBuffer<ParticleDensity> ParticleDensityRO : register( t1 );
			float g_fParticleSize;


			struct appdata
			{
				//float4 vertex : POSITION;
				//float2 uv : TEXCOORD0;
				uint ID : SV_VertexID;
			};

			struct VSParticleOut
			{
    			//float2 position : POSITION;
    			//对于POSITION semantic，sb unity要求必须为float4
    			float4 position : POSITION;
   				float4 color : COLOR;
			};

			struct GSParticleOut
			{
    			float4 position : SV_Position;
    			float4 color : COLOR;
    			float2 texcoord : TEXCOORD;
			};

//--------------------------------------------------------------------------------------
// Visualization Helper
//--------------------------------------------------------------------------------------

static const float4 Rainbow[5] = {
    float4(1, 0, 0, 1), // red
    float4(1, 1, 0, 1), // orange
    float4(0, 1, 0, 1), // green
    float4(0, 1, 1, 1), // teal
    float4(0, 0, 1, 1), // blue
};

float4 VisualizeNumber(float n)
{
    return lerp( Rainbow[ floor(n * 4.0f) ], Rainbow[ ceil(n * 4.0f) ], frac(n * 4.0f) );
}

float4 VisualizeNumber(float n, float lower, float upper)
{
    return VisualizeNumber( saturate( (n - lower) / (upper - lower) ) );
}
//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------

VSParticleOut ParticleVS(uint ID : SV_VertexID)
{
    VSParticleOut Out = (VSParticleOut)0;
    Out.position = float4(ParticlesRO[ID].position,0,1);
    Out.color = VisualizeNumber(ParticleDensityRO[ID].density, 1000.0f, 2000.0f);
    return Out;
}
//--------------------------------------------------------------------------------------
// Particle Geometry Shader
//--------------------------------------------------------------------------------------

static const float2 g_positions[4] = { float2(-1, 1), float2(1, 1), float2(-1, -1), float2(1, -1) };
static const float2 g_texcoords[4] = { float2(0, 1), float2(1, 1), float2(0, 0), float2(1, 0) };

[maxvertexcount(4)]
void ParticleGS(point VSParticleOut In[1], inout TriangleStream<GSParticleOut> SpriteStream)
{
    [unroll]
    for (int i = 0; i < 4; i++)
    {
        GSParticleOut Out = (GSParticleOut)0;
        float4 position = In[0].position + g_fParticleSize * float4(g_positions[i], 0, 0);
        //Out.position = mul(position, UNITY_MATRIX_VP);//sb unity提供的是行矩阵！
        Out.position = mul( UNITY_MATRIX_VP,position);
        Out.color = In[0].color;
        Out.texcoord = g_texcoords[i];
        SpriteStream.Append(Out);
    }
    SpriteStream.RestartStrip();
}


//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------

float4 ParticlePS(GSParticleOut In) : SV_Target
{
    return In.color;
}


			ENDCG
		}
	}
}
